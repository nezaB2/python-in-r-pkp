## Čemu je repozitorij namenjen?

Datotekam in programom, s katerimi bomo delali na projektu “Python in R” v sodelovanju Fakultete za matematiko in fiziko in podjetja B2 izobraževanje in informacijske storitve d.o.o.. Projekt je bil odobren na Javnem razpisu za sofinanciranje projektov Po kreativni poti do znanja (JR 2017/2020) (link:http://www.sklad-kadri.si/si/razpisi-in-objave/novica/n/javni-razpis-po-kreativni-poti-do-znanja-2017-2020/).

Tako bomo lažje delali v skupinah, sledili morebitnim spremembam in aktualnim verzijam.

## Nekaj uporabnih linkov za delo z Gitom in BitBucketom

* https://git-scm.com/
* https://www.atlassian.com/git/tutorials
* https://rogerdudler.github.io/git-guide/

# Kratek vodič skozi Git

## Kaj je git?

Git je program za vodenje različic (revision control system), s katerim skupina
programerjev sodeluje pri pisanju programske opreme. Git hrani zgodovino sprememb, omogoča
vzporedno delo na večih različicah (vejah) izvorne kode in na sploh skrbi, da imajo
programerji usklajene različice.

Git lahko uporabljamo tudi za druge skupne projekte, kot je pisanje knjig in zapiskov
v TeXu. V veliko pomoč je že enemu uporabniku, ki uporablja več računalnikov, saj mu tako
ni treba prenašati dokumentov sem in tja s primitivnimi metodami, kot so elektronska pošta,
USB ključki in Dropbox.

## Kaj potrebujem za delo z Git?

### Programska oprema

Na FMF-jevih računalnikih sta nameščena msysGit in TortoiseGit. Za delo doma pa si
namestite naslednjo opremo:

* OS X: `git` je že nameščen
* Windows: [msysGit](http://msysgit.github.io), za bolj udobno uporabo pa
  [TortoiseGit](https://code.google.com/p/tortoisegit/)
* Linux: namestite si paket `git` ali kakorkoli se mu že reče na vaši distribuciji

Če uporabljate Eclipse, si namestite plugin [EGit](http://eclipse.org/egit/). Ta je
nameščen tudi na Eclipse v FMF-jevih računalniških učilnicah. PyCharm ima podporo za git že
vgrajeno.

### Uporabniški račun na BitBucket

Delali bomo z repozitoriji na [BitBucket](https://bitbucket.org/), zato si tam ustvarite
uporabniški račun, če ga še nimate.

Zaposleni na FMF imajo dostop do [privatnega FMF strežnika](http://git.fmf.uni-lj.si/),
zunanji sodelavci pa ga lahko uporabljajo, če imajo Google account.

## Delo z git

### Mini demo

1. Prijavite se na BitBucket.
2. Ustvarite [nov repozitorij](https://bitbucket.org/repo/create).
    * Repozitorij naj bo javen.
    * Obkljukajte opciji `Issue tracking` in `Wiki`.
    * Izberite programski jezik.
3. Ustvarjeni repozitorij klonirajte na svoj računalnik.
4. V repozitorij dodajte datoteko in vanjo nekaj napišite.
5. Naredite `commit`.
6. Naredite `push`.
7. Datoteka bi se morala pojaviti na strežniku.
8. Pobrišite repozitorij na svojem računalniku.
9. Še enkrat ga klonirajte. Ničesar niste izgubili!

### O še nekaterih možnostih

#### Fork

Če naredite svoj *fork* repozitorija (opcija "Fork" v meniju na levi), s tem dobite svojo različico repozitorija.

#### Clone

Po izvedenem forku se spodobi **svojo različico** repozitorija klonirati (*clone*) k sebi, se pravi ustrezni naslov repozitorija
bo `https://uporabnik@bitbucket.org/uporabnik/python-in-r-pkp.git` kjer `uporabnik` **ni** `nezaB2` (uporabnik, od katerega smo forkali repozitorij).

#### Delajte na svoji različici

#### Commit

Ko ste zadovoljni s spremembami, jih dodajte v repozitorij:

    git commit -m "opis sprememb"

#### Push

Spremembe pošljite na BitBucket (GitHub):

    git push

#### Pull

Če na repozitoriju sodeluje več ljudi, lahko pod forki najdemo seznam uporabnikov (klik na `Forks` na glavnem repozitoriju). 
Posledično si lahko enega izmed njih izberemo in njegove
spremembe prenesemo k sebi. Denimo, da smo izbrali udeleženca z
uporabniškim imenom `lolek`:

    git remote add lolek https://uporabnik@bitbucket.org/lolek/python-in-r-pkp.git
    git pull lolek master

Sedaj imate tudi `lolek`ove datoteke!

#### Pull request

Svoje spremembe predlagajte za vključitev v glavno različico, ki jo ima uporabnik
`nezaB2` (v tem dotičnem primeru). Na svoji različici naredite *pull request* (izbira "pull requests" na
menuju na desni, nato gumb "new pull request"). Z nekaj sreče bo uporabnik `nezaB2`
sprejel vaše spremembe; lahko pa se tudi odloči, da jih bo zavrnil.

#### Pull iz `upstream`

Spremembe, ki jih bo naredil `nezaB2` (sprejel bo kopico pull requestov) prenesite v
svojo različico:

    git remote add upstream https://nezaB2@bitbucket.org/nezaB2/python-in-r-pkp.git
    git pull upstream master

Sedaj imate datoteke vseh sodelujočih!

## Disclaimer

To gradivo je priredba računalniške delavnice o Git-u, ki jo je izvedel prof. dr. Andrej Bauer
in je potekala 28. novembra 2014 na Fakulteti za matematiko in fiziko, Univerza v Ljubljani.
Obdelano je bilo 3. decembra 2014 na vajah dr. Nina Bašića. Sedaj z dodatno predelavo služi  nam kot del navodil za uporabo Gita. :)